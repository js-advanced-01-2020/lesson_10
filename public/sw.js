function precache() {
    return caches.open('v1').then(function (cache) {
        return cache.addAll([
            './images/ca2t.jpg',
            './images/aaa.jpg',
            './images/cat4k.jpg',
            './data/test.json'
        ]);
    });
}


self.addEventListener('install', function(e) {
    console.log('The service worker is being installed.'); 
    e.waitUntil(precache());
});

self.addEventListener('fetch', (event) => {
    event.respondWith(async function() {
        const response = await caches.match(event.request);
        console.log('ev', event.request, response );
        return response || fetch(event.request);
    }());
});





