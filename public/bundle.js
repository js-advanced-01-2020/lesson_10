/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/common/index.js":
/*!*************************************!*\
  !*** ./application/common/index.js ***!
  \*************************************/
/*! exports provided: MakeActive */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _makeBlockActive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./makeBlockActive */ \"./application/common/makeBlockActive.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"MakeActive\", function() { return _makeBlockActive__WEBPACK_IMPORTED_MODULE_0__[\"MakeActive\"]; });\n\n\n\n//# sourceURL=webpack:///./application/common/index.js?");

/***/ }),

/***/ "./application/common/makeBlockActive.js":
/*!***********************************************!*\
  !*** ./application/common/makeBlockActive.js ***!
  \***********************************************/
/*! exports provided: MakeActive */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MakeActive\", function() { return MakeActive; });\nconst MakeActive = id => {\n  const block = document.getElementById(id);\n  block.classList.remove('hidden');\n  console.log('block', block, 'shown');\n};\n\n//# sourceURL=webpack:///./application/common/makeBlockActive.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _sw__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./sw */ \"./application/sw/index.js\");\n// import touch from './touch';\n// import routing from './routing';\n// import sockets from './sockets';\n\ndocument.addEventListener('DOMContentLoaded', () => {\n  // touch();\n  // routing();\n  // sockets();\n  Object(_sw__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n});\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/sw/index.js":
/*!*********************************!*\
  !*** ./application/sw/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common */ \"./application/common/index.js\");\n\n\nconst SW_demo = () => {\n  Object(_common__WEBPACK_IMPORTED_MODULE_0__[\"MakeActive\"])('sw');\n\n  if ('serviceWorker' in navigator) {\n    console.log('IT IS!');\n    window.addEventListener('load', () => {\n      navigator.serviceWorker.register('/sw.js').then(registration => {\n        console.log('SW registered: ', registration);\n      }).catch(registrationError => {\n        console.log('SW registration failed: ', registrationError);\n      });\n    });\n  }\n\n  const load = document.getElementById('load');\n  load.addEventListener('click', () => {\n    let img = new Image();\n    img.src = 'images/cat4k.jpg';\n    console.log('loaded', img);\n    document.body.appendChild(img);\n  });\n  fetch('./data/test.json').then(res => res.json()).then(data => console.log(data));\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SW_demo);\n\n//# sourceURL=webpack:///./application/sw/index.js?");

/***/ })

/******/ });