import { MakeActive } from '../common';

const SW_demo = () => {
    MakeActive('sw');
    
    if ('serviceWorker' in navigator) {
        console.log('IT IS!');
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/sw.js').then(registration => {
                console.log('SW registered: ', registration);
            }).catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
        });
    }    

    const load = document.getElementById('load');

    load.addEventListener('click', () => {
        let img = new Image();
        img.src = 'images/cat4k.jpg';
        console.log('loaded', img);

        document.body.appendChild( img );
    })

    fetch('./data/test.json')
    .then( res => res.json() )
    .then( data => console.log( data ));


}

export default SW_demo;