import { MakeActive } from '../common';

const page1 = () => (`
<div>
    <h1>Page1</h1>
    <button class="_btn">CLICK</button>
</div>
`);
const page2 = () => (`<h1>Page2</h1>`);
const page3 = () => (`<h1>Page3</h1>`);
const notFound = () => (`<h1>Not found</h1>`);



class Page {

    constructor( html, handler ){

        this.html = html;
        this.handler = handler;

        this.render = this.render.bind( this );
        this.initHandler = this.initHandler.bind( this );
        return this.render;
    }

    initHandler(){
        const { className, type, handler } = this.handler;
        
        // console.log( className, this.node.querySelector(`.${className}`) );
        this.node.querySelector(`.${className}`)
            .addEventListener( type, handler );

    }


    render( target ){

        let node = document.createElement('div');

        node.innerHTML = this.html();


        this.node = node;
        target.appendChild( node );



        this.initHandler();
        console.log('render', this )


    }
}


const ROUTES = {
    '/page1': new Page(page1, {
        className: "_btn",
        type: "click",
        handler: () => console.log( 123 )
    }),
    '/page2': new Page(page2),
    '/page3': new Page(page3),
    '/test-page': () => (`
        <div>
            <h1> Hello i'm a test page </h1>
        </div>
    `)
};

const renderRoute = ( url ) => {
    console.log('start render', url, root );
    const rootElement = document.getElementById('root');
    rootElement.innerHTML = null;

    console.log( rootElement )
    try {
        ROUTES[url]( rootElement );
    } catch (error) {
        console.log( error );
        rootElement.innerHTML = notFound();
    }
}

let onNavItemClick = (pathName) => (e) => {
    history.pushState(
        {}, 
        pathName,
        window.location.origin + '#' + pathName
    );

    let url =  location.hash.replace(/#/, '');
    renderRoute( url );
};


const routing = () => {

        MakeActive("navigation");
        console.log( ROUTES );
        window.addEventListener('popstate', (event) => {
            /*
                Цепляемся к событию изменения истории
            */
            let url =  location.hash.replace(/#/, '');
            console.log('location', location, url );
            // console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));

            renderRoute( url );
        });

        window.addEventListener('load', () => {
            let url =  location.hash.replace(/#/, '');
            if( url !== ""){
                console.log('url', url === '');
                renderRoute( url );
            }
        });


        let navs = document.querySelectorAll('._nav');
        navs.forEach( nav => {
            nav.onclick = onNavItemClick( nav.dataset.id );
        });

}


export default routing;