

const Sockets = () => {   
    console.log('ws');

    const root = document.getElementById('root');
    root.innerHTML = `
        <h1> Chat </h1>
        <label>
            <b>Notifications:</b>
            <input type="checkbox" class="_notifications"/> 
        </label>
        <form class="_form">
            <input name="user" class="input _user" required />
            <input name="message" class="input _message" required />
            <button> Send </button>
        </form>
        
        <b> Messages: </b>
        <ul class="_messages">

        </ul>
    `;


    const socket = new WebSocket('ws://localhost:8000');
    const chat = root.querySelector('._messages');
    const form = root.querySelector('._form');

    const writeChat = ( user, text ) => {
        const li = document.createElement('li');
        li.innerHTML = `<b>${user}:</b><span>${text}</span>`;
        chat.appendChild( li );
    }

    form.addEventListener('submit', ( e ) => {
        e.preventDefault();
        let message = form.querySelector('._message');
        let user = form.querySelector('._user');
        let msg = message.value;
        let user_name = user.value;

        message.value = null;
        socket.send(JSON.stringify({
            user: user_name,
            message: msg
        }));
        writeChat( user_name, msg );
    });

    socket.onopen = () => {
        writeChat('system','Someone joined');
    }
    socket.onclose = () => {
        writeChat('system', 'Someone exited');
    }

    socket.onmessage = ( e ) => {
        console.log('data get', e.data );
        let data = JSON.parse( e.data );
        writeChat( data.user, data.message );

        Notification.requestPermission()
            .then( res => {
                console.log( res );
                new Notification(`${data.user}: ${data.message}`);
            });
    }

}

export default Sockets;


/*
    https://caniuse.com/#search=Notifications

   
        
        // const notifications = root.querySelector('._notifications');
     

        notifications.addEventListener('change', () => {

            Notification.requestPermission()
                .then( res => {
                    console.log( res );
                });

        });
*/      