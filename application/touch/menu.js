/*
    События касаний:
    touchstart
    touchmove
    touchend
    touchcancel

    Необходимые свойства для работы с касаниями:
    page.addEventListener('touchstart', (e) => {

        e.touches - Массив в котоом хранятся все касания
        https://developer.mozilla.org/ru/docs/Web/API/TouchEvent/touches

        e.changedTouches - Массив касаний, в котором в зависимости от события
        касания находятся те или иные данные касательно касаний.
        https://developer.mozilla.org/ru/docs/Web/API/TouchEvent/changedTouches
    });

    Библиотека: https://hammerjs.github.io/getting-started/
*/

const Menu = () => {
    
    const page = document.getElementById('page');
    const menu = document.getElementById('menu');

    let startPos;
    let currentPos;
    let threshold = screen.width / 2;


    page.addEventListener('touchstart', (e) => {
        startPos = e.touches[0].clientX;
        menu.style.transition = '0s';
    });

    page.addEventListener('touchmove', (e) => {
         // Отмена действия нужна, что бы не срабатывал нативный скролл
        e.preventDefault();
        // console.log('touchmove', e.touches[0].clientX);

        /*
            Переменные для просчета
            - touchPos - текущая позиция касания
            - change - изменения от начала касания до текущего момента
            - threshold - ширина экрана для свайпа
        */
        let touchPos = e.touches[0].clientX;
        let change = startPos - touchPos;
        /*
            Преобразовать изменение в значение для px
        */
       let positiveChange = change * -1;

        if( startPos > touchPos ){
            /*  
                Движение влево!
                Если наша позиция меньше начальной - значи движение идет влево
            */
            if( currentPos < touchPos ){
                console.log('< then >');
            } else {
                console.log('<');
            }
        } else {
            /*  
                Движение вправо!
                Если наша позиция больше начальной - значи движение идет вправо
            */

            if( currentPos > touchPos){

                console.log('> then <');
                page.style.left = `${ positiveChange }px`;
                // menu.style.left = `-${ screen.width - positiveChange }px`;

                if( positiveChange > threshold  ){
                    page.classList.add('move');
                } else {
                    page.classList.remove('move');
                }
                
            } else {

                console.log('>', change ); 
                /*
                    при движении вправо, change будет уходить в минус
                    нужно преобразовать измененое движение в пиксели сделать из -150 => left: 150px;
                */
                if( change < 0){
                    page.style.left = `${ positiveChange }px`;

                    menu.style.display = 'flex';
                    // ( 375 - (-x * -1 ) ) => 375 - 250 => left : 125px
                    menu.style.left = `-${ screen.width - positiveChange }px`;
                }

                if( positiveChange > threshold  ){
                    page.classList.add('move');
                }
            } 
        }
        
        // обновляем текущее положение для просчета изменений движения
        currentPos = touchPos;

    });

    page.addEventListener('touchend', (e) => {
        /*
            - endPos - Конечная позиция касания
            - change - Конечное изменение
            - threshold - Ширина экрана для свайпа
        */
        let endPos = e.changedTouches[0].clientX;
        let change = startPos - endPos;
        let positiveChange = change * -1;
        //  // После окончания движения переменная с прошлой позицией не нужна - сбрасываем её
        currentPos = null;

        //  // Был ли совершен свайп
        let swipe = positiveChange > threshold ;

        if( swipe ){
            // Если свайп был - додвигаем наш стартовый экран вправо, до упора с анимацией
            page.style.left = '100%';
            page.style.transition = '0.3s';
            // Ставим менюшку на 0px по X
            menu.style.left = `0`;
            menu.style.transition = '0.3s';
        } else {
        //      // Если не хватило для свайпа сбрасываем все на исходную
            page.classList.remove('move');
            page.style.left = 0;
            // page.style.transition = '0.3s';
            menu.style.left = `-100%`;
            // menu.style.transition = '0.3s';
        }

    });

}


export default Menu;