class Slider{

    constructor( id ){
        this.slider = document.getElementById(id);
        this.pos = 0;
        this.prevPos = 0;
        this.touch = {};

        this.init();
    }

    init(){
        this.moveElement = this.slider.querySelector('.slider__row');
        // set start width
        this.itemWidth = this.slider.querySelector('.slider__row').offsetWidth;
        this.fullWidth = this.slider.querySelectorAll('.slider__item').length * this.itemWidth;
        this.handlers();
    }

    handlers(){
        this.slider.querySelector('.slider__nav.next').addEventListener('click', () => this.move(1) );
        this.slider.querySelector('.slider__nav.prev').addEventListener('click', () => this.move(0));
    
        this.slider.querySelector('.slider__row').addEventListener('touchstart', this.touchStart() );
        this.slider.querySelector('.slider__row').addEventListener('touchmove', this.touchMove() );
        this.slider.querySelector('.slider__row').addEventListener('touchend', this.touchEnd() );
    
    }


    move( status ){
        if( status ){
            let newPos = this.pos + this.itemWidth;

            if( newPos >= this.fullWidth){
                newPos = 0;
            }
            this.moveElement.style.transform = `translateX(-${newPos}px)`;
            this.pos = newPos;
        
        } else {
            let newPos = this.pos - this.itemWidth;

            if( newPos < 0){
                newPos = this.fullWidth - this.itemWidth;
            }
            this.moveElement.style.transform = `translateX(-${newPos}px)`;
            this.pos = newPos;
        }
    }


    touchStart(){
        return (e) => {
            console.log('touched start', e);
            this.touch.start = e.changedTouches[0].clientX + this.pos;
        }
    }

    touchMove(){
        return (e) => {
            this.moveElement.style.transition = '0s';
            
            let addedPos = 0;

            addedPos = this.touch.start - e.changedTouches[0].clientX;
            
            
            if( addedPos > this.touch.start){
                // Move right
                console.log('>')
            } else {
                // Move left
                console.log('<')
                addedPos = addedPos * -1;
            }
            console.log('touched move', addedPos );
            this.moveElement.style.transform = `translateX(${addedPos}px)`;   
        }
    }

    touchEnd(){
        return (e) => {
            const limit = screen.width / 3;

            if( this.touch.added < limit ){
                this.moveElement.style.transition = '0.3s';
                this.moveElement.style.transform = `translateX(${this.pos}px)`;
            } else {
                console.log('move next');
                this.moveElement.style.transition = '0.3s';
                this.move(1);
            }

        }
    }

}


export default Slider;