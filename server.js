'use strict';

const express = require('express');
const fs = require('fs');
const http = require('http');
// const Websocket = require('websocket').server;
const WebSocket = require('ws');

const app = express();
const port = 3000;

app.use( express.static('public') );
app.get('*', (req, res) => res.send('/public/index.html'));
app.listen(port, () => console.log(`Example app listening on port ${port}!`));

const wss = new WebSocket.Server({ port: 8000 });
const clients = [];

wss.on('connection', function connection(ws) {
    ws.on('open', function open(){
        console.log('user join')
        ws.send(JSON.stringify({ user: 'System', message: "Someone join"}));
    });
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
        wss.clients.forEach( client => {
            if( client !== ws && client.readyState === WebSocket.OPEN){
                client.send( message );
            }

        })
    });
});